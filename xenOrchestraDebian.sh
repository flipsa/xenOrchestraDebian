#!/bin/bash

# Xen Orchestra Installer by Stefan Heil (heil@tu-berlin.de)
# Version 0.3.5

# Changelog:
#
# 0.3.5
# - Bugfix: Added missing dep: fuse, cifs-utils
# - Change: using "git checkout ." instead of "git stash" before updating
#
# 0.3.4
# - Upgrade: nodejs v8 (new LTS version)
#
# 0.3.3
# - Bugfix: fixed plugin linking on update
#
# 0.3.2
# - Bugfix: fixed plugin linking on first install
# - Bugfix: fixed yarn not buing installed on first install
#
# 0.3.1
# - Bugfix: added missing dependency for lvm2


# This script will install Xen Orchestra on your system.
# Currently only Debian >9 (Stretch) and Ubuntu >16.10 are tested, but other
# Debian-based systems will probably work, too.
# If you want support for another system, please send a merge request

# Define paths - NO TRAILING SLASH/
XO_PATH="/srv/xo"
XO_DATA_DIR_NAME="xo-data"
XO_UNIFIED_DIR_NAME="xen-orchestra"

# Define user and group
XO_USER="root"
XO_GROUP="root"

CHECK_DEP() {
    # Check for dependencies
    if [ -z "$1" ] # Is parameter #1 zero length?
    then
        printf "This function expects exactly one argument: the name of the dependency / utility\n"
        printf "This is an internal error and should not happen. Blame your admin.\n\n"
        exit 1
    else
        # Assign variable value from input to this function
        UTIL=$1

        printf "#######################################\n\n"
        printf "Checking dependencies for '$UTIL'\n\n"
    fi

    # Check if the utility exists on our system / in our path
    which $UTIL > /dev/null 2>&1

    # If the last command didn't succeed (i.e. returned a non-zero exit code, aka an error), then...
    if [ $? -ne 0 ]
    then
        printf "The '$UTIL' utility was not found on your system. Installing it now!\n\n"

        case $UTIL in
            npm)
                UTIL_PACKAGE=nodejs
                # Add repo for nodejs
                # Xen Orchestra website says to only use the latest LTS version. Currently 8.x
                curl -sL https://deb.nodesource.com/setup_8.x | bash -  # I'm living dangerously... ¯\_(ツ)_/¯
                ;;
             yarn)
				UTIL_PACKAGE=$UTIL
                # Add repo for yarn package manager for nodejs
                curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
                printf "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
                apt-get update
                ;;
            *)
                # All other packages we need, as long as they fulfill these requirements:
                # i) package name == utility's name
                # ii) must be installable through official package manager (only Debian / apt supported right now)
                # if that is not the case, you need to write another 'case' statement above
                UTIL_PACKAGE=$UTIL
                ;;
        esac

        # Install the package with apt
        apt-get install -y "$UTIL_PACKAGE"

        # If the last command didn't succeed (i.e. returned a non-zero exit code, aka an error), then...
        if [ $? -ne 0 ]
        then
            printf "\n\nSomething went wrong during installation of '$UTIL'. Giving up now!\n\n"
            printf "#######################################\n\n"
            exit 1
        fi

    fi

    # Check if the utility is now found in path
    UTIL_BIN=$(which $UTIL 2> /dev/null)

    # Check if the utility is installed correctly / executable
    if [[ -x "$UTIL_BIN" ]]
    then
        VERSION=$($UTIL_BIN --version)
        printf "'$UTIL' is installed in '$UTIL_BIN' and executable!\n\n"
        printf "Version: $VERSION\n\n"
        printf "#######################################\n\n"
    else
        printf "#######################################\n\n"
        printf "'$UTIL' is not executable or not found, giving up now!\n\n"
        printf "#######################################\n\n"
        exit 1
    fi
}

CHECK_ALL_DEPS() {
    # Check for dependencies
    printf "#######################################\n\n"
    printf "Checking for Xen Orchestra dependencies\n\n"

    CHECK_DEP curl
    CHECK_DEP git
    CHECK_DEP redis-server
    CHECK_DEP npm
    CHECK_DEP yarn
}

LINK_PLUGINS() {
    # Since Xen Orchestra moved to a mono-repo approach, the plugins are built
    # automatically, but they still need to be linked to the right location.
    # see: https://xen-orchestra.com/forum/post/4841

    printf "#######################################\n\n"
    printf "Linking Xen Orchestra Plugins\n\n"

    for i in =$(ls -d $XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server-*); do
        ln -s "$i" $XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server/node_modules/
    done
}

GEN_SSL_CERT(){
    printf "#######################################\n\n"
    printf "Generating OpenSSL certificate for https encryption\n\n"
    cd $XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server/
    openssl genrsa -out xoa-key.pem 4096
    openssl req -new -sha256 -key xoa-key.pem -out xoa-csr.pem
    openssl x509 -req -in xoa-csr.pem -signkey xoa-key.pem -out xoa-cert.pem
}

SYSTEMD_STARTSCRIPT(){
    # Check if a systemd start script already exists, if not create it:

    if [[ ! -e /etc/systemd/system/xenOrchestra.service ]] ; then

    # Create a systemd unit file
    cat > /etc/systemd/system/xenOrchestra.service <<__EOF__
[Unit]
Description=Xen Orchestra web GUI for XenServer and Xen
After=network-online.target

[Service]
WorkingDirectory=$XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server
ExecStart=$XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server/bin/xo-server
Restart=always
SyslogIdentifier=xen-orchestra
User=$XO_USER
Group=$XO_GROUP

[Install]
WantedBy=multi-user.target
__EOF__

    fi

    # Start automatically at boot
    systemctl enable xenOrchestra.service

    # Start the service
    systemctl restart xenOrchestra.service
}

INITIAL_CONFIG() {
    # Create a sample config and configure some variables
    cd $XO_PATH/$XO_UNIFIED_DIR_NAME/packages/xo-server

    # Copy the sample config
    cp sample.config.toml .xo-server.toml

    # Replace default values with our own
    sed -i "s/#user: 'nobody'/user: 'nobody'/" .xo-server.toml
    sed -i "s/#group: 'nogroup'/group: 'nogroup'/" .xo-server.toml
    sed -i "s/port: 80/#port: 8080/" .xo-server.toml
    sed -i "s/#   port: 443/    port: 443/" .xo-server.toml
    sed -i "s/#   cert: '.\/certificate.pem'/    cert: '.\/xoa-cert.pem'/" .xo-server.toml
    sed -i "s/#   key: '.\/key.pem'/    key: '.\/xoa-key.pem'/" .xo-server.toml
    sed -i "s,#'/': '/path/to/xo-web/dist/','/': '../xo-web/dist/'," .xo-server.toml
    sed -i "s,#datadir: '/var/lib/xo-server/data',datadir: '/srv/xo/xo-data'," .xo-server.toml
    #sed -i "s,#socket: /var/run/redis/redis.sock,socket: /var/run/redis/redis.sock," .xo-server.toml
}

# Actual script to install or update Xen Orchestra
case "$1" in

    install)
        printf "#######################################\n\n"
        printf "Installing Xen Orchestra, please hang on ...\n\n"
        printf "#######################################\n\n"

        # Install Required packages
        # Not sure if blktap-utils is really needed or not...
        apt-get install -y build-essential libpng-dev python-minimal lvm2 libvhdi-utils blktap-utils fuse cifs-utils

        # Check for required dependencies
        CHECK_ALL_DEPS

        # Setup directory and add the xo user
        mkdir $XO_PATH
        adduser $XO_USER --no-create-home --quiet --home /srv/$XO_PATH --shell /bin/bash --disabled-password --system -U

        # Clone the archives (XenOrchestra uses a mono-repo as of 2018-02-01!)
        cd $XO_PATH
        git clone -b master http://github.com/vatesfr/xen-orchestra
        chown -R $XO_USER:$XO_GROUP $XO_PATH

        # Build dependencies with yarn
        cd $XO_UNIFIED_DIR_NAME
        yarn && yarn build

        # Link the plugins after calling yarn / yarn build
        LINK_PLUGINS

        # Generate SSL Certificate
        GEN_SSL_CERT

        # Create our initial config from the sample config
        INITIAL_CONFIG

        # Create and chown data directory
        mkdir $XO_PATH/$XO_DATA_DIR_NAME
        chown "$XO_USER":"$XO_GROUP" $XO_PATH/$XO_DATA_DIR_NAME -R

        # Check if we already have a systemd unit file and create it if not
        SYSTEMD_STARTSCRIPT

        # Print success message and exit
        printf "#######################################\n\n"
        printf "Installation complete, now go to: " && hostname -I && echo "" && echo "Default Login: "admin@admin.net"" && echo "" && echo "Password: admin" && echo "" && echo "Don't forget to change your password!"

        exit 0

    ;;

    update)
        # Update the system through the package manager.
        printf "#######################################\n\n"
        printf "Updating system packages through package manager, please wait ...\n\n"

        apt-get update && apt-get upgrade -y

        # Check for dependencies
        CHECK_ALL_DEPS

        # Update XO Server
        printf "\n\n#######################################\n\n"
        printf "Updating Xen Orchestra Server ...\n\n"

        # Change to xen-orchestra directory
        cd $XO_PATH/$XO_UNIFIED_DIR_NAME

        # Stash any changed files and pull updates
        #git stash
        git checkout .
        git pull --ff-only

        # Build from sources
        yarn && yarn build

        # Link the plugins after calling yarn / yarn build
        LINK_PLUGINS

        # Restart the server
        printf "\n\n#######################################\n\n"
        printf "Restarting Xen Orchestra Server\n\n"
        systemctl restart xenOrchestra.service

        # Print success messages and exit
        printf "\n\n#######################################\n\n"
        printf "Finished updating Xen Orchestra - NICE\n\n"
        printf "Depending on your system, you might have to wait a little bit till the server is restarted. Then refresh your web browser and hope for the best...\n\n"
    ;;

    #debug)
        # Start the backend in the foreground (for testing)
        #yarn start
    #;;

    *)
        echo "Not enough arguments given!"
        echo "Specify either 'install' or 'update'"
    ;;

esac
