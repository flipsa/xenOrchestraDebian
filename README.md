# Xen Orchestra Debian Installer / Updater

## Description

This is a shell script to automatically install or update Xen Orchestra from the sources

## Compatibility

Tested on the following distributions and architectures

Works on:
- Debian 9.2 (Stretch) / amd64
- Debian 9.1 (Stretch) / amd64
- Ubuntu 16.10 (Yakkety Yak) / amd64

If you try this script on another distro / architecture let me know so I can update this list.

## Usage

The script needs to be adapted to your environment by setting a few variables (see beginning of the script)

Define paths (NO TRAILING SLASH/):
XO_PATH="/srv/xo"
XO_DATA_DIR_NAME="xo-data"
XO_UNIFIED_DIR_NAME="xen-orchestra"

Define user and group
XO_USER="root"
XO_GROUP="root"


### Installation

`./xenOrchestraDebian.sh install`

### Update

`./xenOrchestraDebian.sh update`